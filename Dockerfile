# Usamos la imagen oficial de Jenkins disponible den Dockerhub
# Por defecto, su base será Ubuntu.
FROM jenkins/jenkins:latest
ENV JENKINS_USER admin
ENV JENKINS_PASS admin
LABEL maintainer="fdurante@alu.ucam.edu"

# Instalamos el motor de Docker e incluimos el usuario por
# defecto 'jenkins' en el grup ode seguridad 'docker'.
USER root
RUN apt-get update && apt-get install -y lsb-release 
RUN curl -4fsSLo /usr/share/keyrings/docker-archive-keyring.asc \
  https://download.docker.com/linux/debian/gpg
RUN echo "deb [arch=$(dpkg --print-architecture) \
  signed-by=/usr/share/keyrings/docker-archive-keyring.asc] \
  https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
RUN apt-get update && apt-get install -y docker-ce-cli
RUN groupadd docker
RUN usermod -aG docker jenkins

# Deshabilitamos la página de bienvenida 'SetupWizard'.
USER jenkins

# Skip initial setup
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"

# Establecemos el usuario admin por defecto y su password
# Usaremos el fichero default-user.groovy.
COPY default-user.groovy /usr/share/jenkins/ref/init.groovy.d/

# Indicamos algunos plugins que nos interesan se instales por defecto.
COPY plugins.txt /usr/share/jenkins/ref/
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
